// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="how-to-openshift-deploy"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= How to deploy Stasis Reactor on OpenShift
:stasis-tags: Deployment, Kubernetes
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

This repository is ready to be deployed on OpenShift with just a few easy
commands.

.Prerequisites

* a terminal shell with access to the OpenShift client(`oc`) available.
* an active login to an OpenShift cluster available.
* a web browser available.

.Procedure

. Begin the application build and deployment.
+
....
oc new-app centos/nodejs-10-centos7~https://gitlab.com/elmiko/stasis-reactor.git
....

. Expose a route to your deployment.
+
....
oc expose svc/stasis-reactor
....


.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.

. Get the route for your deployed Stasis Reactor.
+
....
oc get route stasis-reactor
....

. Open the listed route in your web browser. You should now see the Stasis
  Reactor documentation site.

.Additional resources

* link:how-to-add-content-openshift.html[How to add custom content in OpenShift]
* https://odk.io[OpenShift documentation]
* https://www.okd.io/download.html[OpenShift client tool downloads]
