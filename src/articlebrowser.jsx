import React from 'react';
import ArticleIndex from './articleindex';
import ArticleTagFilter from './articletagfilter';
import ArticleTitleFilter from './articletitlefilter';

export class ArticleBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      doclist: Array.from(this.props.docindex.documents),
      taglist: [],
      titleFilter: ""
    };
    this.updateTagFilter = this.updateTagFilter.bind(this);
    this.updateTitleFilter = this.updateTitleFilter.bind(this);
  }

  filterDocList(docs, title, tags) {
    let newlist;

    if (title !== "") {
      newlist = docs.filter(doc => doc.title.toLowerCase().includes(title.toLowerCase()));
    } else {
      newlist = Array.from(docs);
    }

    if (newlist.length > 0) {
      newlist = newlist.filter(
        doc => tags.filter(tag => doc.tags.includes(tag)).length === tags.length
      );
    }

    return newlist;
  }

  updateTagFilter(value) {
    let newtaglist;
    if (this.state.taglist.includes(value)) {
      newtaglist = this.state.taglist.filter(tag => tag != value);
    } else {
      newtaglist = Array.from(this.state.taglist);
      newtaglist.push(value);
    }

    let newdoclist = this.filterDocList(
      this.props.docindex.documents,
      this.state.titleFilter,
      newtaglist);

    this.setState({
      doclist: newdoclist,
      taglist: newtaglist
    });
  }

  updateTitleFilter(value) {
    let newdoclist = this.filterDocList(
      this.props.docindex.documents,
      value,
      this.state.taglist);

    this.setState({
      doclist: newdoclist,
      titleFilter: value
    });
  }


  render() {
    return (
      <div>
        <div>
          <h1>Filters</h1>
          <ArticleTitleFilter onChange={this.updateTitleFilter} />
          <br/>
          <ArticleTagFilter tags={this.props.docindex.tags} onChange={this.updateTagFilter} />
        </div>
        <ArticleIndex documents={this.state.doclist} />
      </div>
    );
  }
}

export default ArticleBrowser;
