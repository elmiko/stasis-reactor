import React from 'react';
import TagSelector from './tagselector';

export class ArticleTagFilter extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(tag) {
    this.props.onChange(tag);
  }

  render() {
    const tags = this.props.tags.sort().map((tag, index) =>
      <TagSelector tag={tag} key={index} handleClick={this.handleClick} />
    );
    return (
      <div><label>Tags</label>{tags}</div>
    );
  }
}

export default ArticleTagFilter;


