import React from 'react';

export class TagSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    var newstate = !this.state.selected;
    this.setState({"selected": newstate});
    this.props.handleClick(this.props.tag);
  }

  render() {
    let className = this.state.selected === true ? "tag-selected" : "tag-unselected";

    return (
      <button className={className} onClick={this.handleClick}>{this.props.tag}</button>
    );
  }
}

export default TagSelector;


