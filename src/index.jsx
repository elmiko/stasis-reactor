import React from 'react';
import ReactDOM from 'react-dom';
import ArticleBrowser from './articlebrowser'
import './tagselector.css';

fetch('docs/index.json', {cache: "no-store"})
  .then(function(response) {
    return response.json();
  })
  .then(function(docsjson) {
    const docindex = docsjson.index;
    ReactDOM.render(
      <ArticleBrowser docindex={docindex}/>,
      document.getElementById('app')
    );
  });
