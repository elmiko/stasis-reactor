# stasis reactor

This is a project for presenting a series of [AsciiDoc](https://asciidoc.org)
files in a statically generated website with a [React](https://reactjs.org)-based
user interface.

This application is self-documenting, with the example content in the
[`docs` directory](/docs) containing all the project documentation.
