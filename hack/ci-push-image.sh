#!/bin/sh
set -x

if [ $CI_COMMIT_REF_NAME == "master" ]
then
docker login -u elmiko+stasisbot -p $SR_REGISTRY_TOKEN $SR_TARGET_REGISTRY
docker tag stasis-reactor:$CI_COMMIT_REF_NAME $SR_TARGET_REGISTRY:latest
docker push $SR_TARGET_REGISTRY:latest
fi

